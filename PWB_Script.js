const blocklisturl = chrome.extension.getURL("PWB_Blocklist.json")
const blocklistdata = fetchBlocklist(blocklisturl);

// Load the PWB_Blocklist.json
blocklistdata.then(function (config) {
    newspaper = identifyDomain(config)
    if (newspaper) {
        findArticles(config[newspaper])
    } else {
        logging("No suitable Blocklist for this Domain")
    }
})

function identifyDomain(config) {
    let domain = document.URL
    for (const searchdomain in config) {
        if (domain.indexOf(searchdomain) != -1 ) {
            return searchdomain
        }
    }
    return false
}

function findArticles(searchList) {
    let counter = 0;
    searchList.forEach(settings => {
        const search = settings['searchElement'];
        const searchClosest = settings['closestElement'];
        const searchElements = document.querySelectorAll(search);

        for (let i = 0; i < searchElements.length; i++) {
            const found = searchElements[i];
            const closest = found.closest(searchClosest)
            if (closest) {
                counter++;
                closest.classList.add('PayWallBlocker');
            }
        }
    });
    logging("hid " + counter + " elements.")
}

function fetchBlocklist(url){
    return fetch(url)
    .then(response => response.json())
    .then(function(response) { return response; })
    .catch(function(error) {
        logging(error);
    });    
}

function logging(message) {
    console.log('PayWallBlocker: ' + message);
}