# PayWallBlocker
This extension will help you stop being bothered by annoying PayWall articles.

Paywall Blocker achieves this by hiding the most commonly referred to as Plus articles on the overview pages. In the current version, the visibility of these articles is lowered and when you move the mouse over the article, it is displayed again and you can view and handle it like any other article. 

Paywall Blocker supports the largest German online magazines, newspapers and news sites. 

It should be working in all Chrome and Firefox based Browsers. 

Tested in the following Browsers:
- Chrome (88.0.4324.190)
- Brave (1.20.110 Chromium: 88.0.4324.192)
- Edge (88.0.705.81)
- Opera (74.0.3911.160)
- Firefox (86.0), works but needs an other installation as described here.


## Installation
The Extension is now in the greatest Web Extension Stores available

### Chrome / Brave
https://chrome.google.com/webstore/detail/paywall-blocker/oifbpnmoaigfdjgpndhealhnnbbncmon?hl=de&authuser=0

### Firefox
https://addons.mozilla.org/de/firefox/addon/paywallblocker/

### Opera
https://addons.opera.com/de/extensions/details/paywallblocker/

### Edge
https://microsoftedge.microsoft.com/addons/detail/paywall-blocker/dagcjdflenhjnlemkocanmiggnknonci

### Manual Installation Chrome based Browser
1. Download this extension
2. Open your Browser Extensions Panel. Normaly you can open [chrome://extensions]
3. Activate the Developer Mode
4. Click on "Install unpacked Extension"
5. Point to the Downloaded Extension Folder
6. See the Magic Happens

## Working Magazines
By now this Extension blocks the Paywall'ed Articles of the following Magazines:
moved to wiki
https://codeberg.org/dasistdaniel/PayWallBlocker/wiki/Working-URLs

## Todo
- [ ] Add Settings Page
- [X] Add more Blocklist
- [ ] Add online Updater
- [X] Create Logo
- [X] Make it into the Extension Stores